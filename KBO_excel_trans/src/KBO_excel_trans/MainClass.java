package KBO_excel_trans;

import java.io.EOFException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.util.ArrayList;

public class MainClass {
	public static void main(String [] ar) throws IOException{
		FileOutputStream out = new FileOutputStream("c:/out2.txt");
		ArrayList<ArrayList<String>> repairTable = new ArrayList<ArrayList<String>>();
		
		textFileImporter a = new textFileImporter("2015_deagam_p315-p1048.txt");
		repairTable = a.splitWord();
		repairTable = a.mergeString_space(repairTable);
		repairTable = a.mergeString_oneword(repairTable);
		repairTable = a.setString_horizontal(repairTable);
		repairTable = a.mergeString_plus(repairTable);
		repairTable = a.deleteString_ect(repairTable);
		
		for(int i = 0; i < 31382; i++){
			for(int j = 0; j < repairTable.get(i).size(); j++){
				String data = repairTable.get(i).get(j) + " ";
				out.write(data.getBytes());
			}
			out.write("\r\n".getBytes());
			//System.out.println();
		}
		out.close();
	}
}
