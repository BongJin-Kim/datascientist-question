package KBO_excel_trans;

import java.io.*;
import java.util.ArrayList;

public class textFileImporter {
	/*
	 * 입력받은 텍스트 파일 저장소 선언
	 */
	private ArrayList<String> inputString = new ArrayList<String>();
	
	private ArrayList<String> splitWordTemplete = new ArrayList<String>();
	
	private ArrayList<ArrayList<String>> repairedTemplete = new ArrayList<ArrayList<String>>();
	
	/*
	 * 생성자.
	 * UTF-8 char-set으로 파라메터 파일을 읽고,
	 * 읽은 파일을 라인별로 위의 저장소에 배열형태로 저장함
	 */
	public textFileImporter(String inputfile){
		try {
			BufferedReader in = 
					new BufferedReader(new InputStreamReader(new FileInputStream(inputfile),"UTF-8"));
			
			String tempString;
			while(true){
				tempString = in.readLine();
				
				if(tempString == null){
					break;
				}
				inputString.add(tempString);
			}
			in.close();
		} 
		catch(IOException io){
			System.err.println("invalid input file!");
			System.exit(0);
		}
	}
	
	public ArrayList<ArrayList<String>> splitWord(){
		/*
		 * 각 라인 스트링을 잘게 나누어 불필요한 스페이스를 제거
		 */
		int count = 0;
		for(int i = 0; i < this.inputString.size(); i++){
			count = 0;
			for(int j = 0; j < this.inputString.get(i).split(" ").length; j++){
				this.splitWordTemplete.add(this.inputString.get(i).split(" ")[j]);
			}
			int templeteSize = this.splitWordTemplete.size();
			for(int k = 0; k < templeteSize; k++){
				if(this.splitWordTemplete.get(count).equals("")){
					this.splitWordTemplete.remove(count);
				}
				else{
					//System.out.println(this.splitWordTemplete.get(count));
					count++;
				}
			}
			ArrayList<String> clone = (ArrayList<String>)this.splitWordTemplete.clone();
			this.repairedTemplete.add(clone);
			this.splitWordTemplete.clear();
		}
		
		return this.repairedTemplete;
	}
	
	/*
	 * 라인별 불필요한 개행 등을 정리
	 */
	public ArrayList<ArrayList<String>> mergeString_space(ArrayList<ArrayList<String>> inputFile){
		int arraySize = inputFile.size();
		int count = 0;
		for(int i = 0; i < arraySize; i++){
			try{
			if(inputFile.get(count).size() == 0){
				inputFile.remove(count);
			}else if(inputFile.get(count).get(0).contains(Keyword.mark2)){
				inputFile.remove(count);
			}else if(inputFile.get(count).get(0).contains("PART")){
				inputFile.remove(count);
			}else if(inputFile.get(count).get(2).contains("기록대백과")){
				inputFile.remove(count);
			}
			else{
				count++;
			}
			}catch(IndexOutOfBoundsException e){
				count++;
			}
			
		}
		
		return inputFile;
	}
	/*
	 * 세로 글자를 가로로 전환
	 */
	public ArrayList<ArrayList<String>> setString_horizontal(ArrayList<ArrayList<String>> inputFile){
		int arraySize = inputFile.size();
		int count = 0;
		boolean check2000 = false;
		boolean checkName = false;

		String rowString = "";
		ArrayList<String> testArr  = new ArrayList<String>();
		ArrayList<String> testArr2  = new ArrayList<String>();
		ArrayList<String> testArr3  = new ArrayList<String>();
		ArrayList<String> testArr4  = new ArrayList<String>();
		ArrayList<String> testArr5 = new ArrayList<String>();
		ArrayList<String> testArr6 = new ArrayList<String>();
		ArrayList<String> testArr7 = new ArrayList<String>();

		testArr.add("팀");
		testArr.add("평균자책점");testArr.add("경기수");testArr.add("완봉승");testArr.add("완투승");testArr.add("승리");
		testArr.add("패전");testArr.add("무승부");testArr.add("세이브");testArr.add("홀드");testArr.add("승률");
		testArr.add("투구이닝");testArr.add("투구수");testArr.add("타자");testArr.add("타수");testArr.add("피안타");
		testArr.add("피홈런");testArr.add("희타");testArr.add("희비");testArr.add("4구");testArr.add("사구");
		testArr.add("탈삼진");testArr.add("폭투");testArr.add("보크");testArr.add("실점");testArr.add("자책점");
		
		testArr2.add("팀");testArr2.add("타율");testArr2.add("경기수");testArr2.add("타석");testArr2.add("타수");
		testArr2.add("득점");testArr2.add("안타");testArr2.add("2루타");testArr2.add("3루타");testArr2.add("홈런");
		testArr2.add("루타");testArr2.add("타점");testArr2.add("도루");testArr2.add("도실");testArr2.add("희타");
		testArr2.add("희비");testArr2.add("4구");testArr2.add("사구");testArr2.add("삼진");testArr2.add("병살타");
		testArr2.add("잔루");testArr2.add("실책");
		
		testArr3.add("팀");testArr3.add("경기수");testArr3.add("평균자책점");testArr3.add("완투");testArr3.add("완봉");
		testArr3.add("승리");testArr3.add("패전");testArr3.add("세이브");testArr3.add("투구이닝");testArr3.add("투구수");
		testArr3.add("타자");testArr3.add("타수");testArr3.add("피안타");testArr3.add("피홈런");testArr3.add("희타");
		testArr3.add("희비");testArr3.add("4구");testArr3.add("고의4구");testArr3.add("사구");testArr3.add("탈삼진");
		testArr3.add("폭투");testArr3.add("보크");testArr3.add("실점");testArr3.add("자책점");
		
		testArr4.add("팀");testArr4.add("경기수");testArr4.add("타율");testArr4.add("타석");testArr4.add("타수");
		testArr4.add("득점");testArr4.add("안타");testArr4.add("2루타");testArr4.add("3루타");testArr4.add("홈런");
		testArr4.add("루타");testArr4.add("타점");testArr4.add("도루");testArr4.add("도실");testArr4.add("희타");
		testArr4.add("희비");testArr4.add("4구");testArr4.add("고의4구");testArr4.add("사구");testArr4.add("삼진");
		testArr4.add("병살타");testArr4.add("잔루");testArr4.add("실책");testArr4.add("출루율");testArr4.add("장타율");
		testArr4.add("수비율");
		
		testArr5.add("연도");testArr5.add("소속");testArr5.add("경기수");testArr5.add("완투");testArr5.add("완봉");
		testArr5.add("선발");testArr5.add("종료");testArr5.add("승리");testArr5.add("구원승");testArr5.add("패전");
		testArr5.add("세이브");testArr5.add("홀드");testArr5.add("투구이닝");testArr5.add("타자");testArr5.add("타수");
		testArr5.add("피안타");testArr5.add("피홈런");testArr5.add("희타");testArr5.add("희비");testArr5.add("4구");
		testArr5.add("고의4구");testArr5.add("사구");testArr5.add("폭투");testArr5.add("보크");testArr5.add("탈삼진");
		testArr5.add("실점");testArr5.add("자책점");testArr5.add("WHIP");testArr5.add("평균자책점");testArr5.add("승률");
		testArr5.add("수비율");
		
		testArr6.add("팀");testArr6.add("경기수");testArr6.add("평균자책점");testArr6.add("완투");testArr6.add("완봉");
		testArr6.add("승리");testArr6.add("패전");testArr6.add("세이브");testArr6.add("홀드");testArr6.add("투구이닝");
		testArr6.add("투구수");testArr6.add("타자");testArr6.add("타수");testArr6.add("피안타");testArr6.add("피홈런");
		testArr6.add("희타");testArr6.add("희비");testArr6.add("4구");testArr6.add("고의4구");testArr6.add("사구");
		testArr6.add("탈삼진");testArr6.add("폭투");testArr6.add("보크");testArr6.add("실점");testArr6.add("자책점");
		
		testArr7.add("연도");testArr7.add("소속");testArr7.add("경기수");testArr7.add("타석");testArr7.add("타수");
		testArr7.add("득점");testArr7.add("안타");testArr7.add("2루타");testArr7.add("3루타");testArr7.add("홈런");
		testArr7.add("루타수");testArr7.add("타점");testArr7.add("4구");testArr7.add("고의4구");testArr7.add("사구");
		testArr7.add("삼진");testArr7.add("희타");testArr7.add("희비");testArr7.add("도루");testArr7.add("도실");
		testArr7.add("병살타");testArr7.add("실책");testArr7.add("타율");testArr7.add("장타율");testArr7.add("출루율");
		testArr7.add("OPS");testArr7.add("도루성공률");testArr7.add("수비위치별출장수");
		
		for(int i = 0; i < arraySize; i++){
			try{
				for(int j = 0; j < 2; j++){
					rowString += inputFile.get(count).get(j);
				}
				if(rowString.contains("2000시즌")){
					check2000 = true;
				}else if(rowString.contains("히메네스")){
					checkName = true;
				}
					if(rowString.contains("평경")){
						inputFile.set(count,testArr);
						count++;
						rowString="";
						inputFile.remove(count);
						inputFile.remove(count);
						inputFile.remove(count);
						inputFile.remove(count);
						//System.out.print(rowString);
					}else if(rowString.contains("타경")){
						inputFile.set(count,testArr2);
						count++;
						rowString="";
						inputFile.remove(count);
						inputFile.remove(count);
					}else if(rowString.contains("경평")&&check2000==false){
						inputFile.set(count,testArr3);
						count++;
						rowString="";
						inputFile.remove(count);
						inputFile.remove(count);
						inputFile.remove(count);
						inputFile.remove(count);
						//System.out.print(check2000);
					}else if(rowString.contains("경타")){
						inputFile.set(count,testArr4);
						count++;
						rowString="";
						inputFile.remove(count);
						inputFile.remove(count);
						inputFile.remove(count);
						inputFile.remove(count);
					}else if(rowString.contains("경평")&&check2000==true){
						inputFile.set(count,testArr6);
						count++;
						rowString="";
						inputFile.remove(count);
						inputFile.remove(count);
						inputFile.remove(count);
						inputFile.remove(count);
						//System.out.print(check2000);
					}else if(rowString.contains("연소")&&checkName==false){
						inputFile.set(count,testArr5);
						count++;
						rowString="";
						inputFile.remove(count);
						inputFile.remove(count);
						inputFile.remove(count);
						inputFile.remove(count);
					}else if(rowString.contains("연소")&&checkName==true){
						inputFile.set(count,testArr7);
						count++;
						rowString="";
						inputFile.remove(count);
						inputFile.remove(count);
						inputFile.remove(count);
						inputFile.remove(count);
					}else{
					}
						count++;
						rowString="";
					
				
			}catch(IndexOutOfBoundsException e){
				count++;

			}

		}
		return inputFile;
	}
	
	/*
	 * 단일 셀에 들어갈 글자가 여러개로 인식된 경우의 수에 대해 붙임 글자로 대체
	 */
	public ArrayList<ArrayList<String>> mergeString_oneword(ArrayList<ArrayList<String>> inputFile){
		int arraySize = inputFile.size();
		Keyword word = new Keyword();
		for(int i = 0; i < arraySize; i++){

			for(int j = 0; j < inputFile.get(i).size(); j++){
				try{
				if(inputFile.get(i).get(j).length() == 1 && j+1 < inputFile.get(i).size()){
					if(word.keywordDic.contains(inputFile.get(i).get(j) + inputFile.get(i).get(j+1))){
						inputFile.get(i).set(j,inputFile.get(i).get(j) + inputFile.get(i).get(j+1));
						inputFile.get(i).remove(j+1);
					}else if (word.keywordDic.contains(inputFile.get(i).get(j) + 
							inputFile.get(i).get(j+1) + inputFile.get(i).get(j+2))){
						inputFile.get(i).set(j,inputFile.get(i).get(j) 
								+ inputFile.get(i).get(j+1) + inputFile.get(i).get(j+2));
						inputFile.get(i).remove(j+1);
						inputFile.get(i).remove(j+1);
					}
						
				}
				}catch(IndexOutOfBoundsException e){}
				if(inputFile.get(i).get(j).length() == 2 && j+1 < inputFile.get(i).size()-1){
					if(word.keywordDic.contains(inputFile.get(i).get(j) + inputFile.get(i).get(j+1)
							 + inputFile.get(i).get(j+2))){
						inputFile.get(i).set(j,inputFile.get(i).get(j) + inputFile.get(i).get(j+1)
								+ inputFile.get(i).get(j+2));
						inputFile.get(i).remove(j+1);
						inputFile.get(i).remove(j+1);
						//System.out.println(inputFile.get(i).get(j));
					}
				}
				if(inputFile.get(i).get(j).length() == 2 && j+1 < inputFile.get(i).size()){

					if(word.keywordDic.contains(inputFile.get(i).get(j) + inputFile.get(i).get(j+1))){
						inputFile.get(i).set(j,inputFile.get(i).get(j) + inputFile.get(i).get(j+1));
						inputFile.get(i).remove(j+1);

					}
				}
				if(inputFile.get(i).get(j).length() == 3 && j+1 < inputFile.get(i).size()-1){
					if(word.keywordDic.contains(inputFile.get(i).get(j) + inputFile.get(i).get(j+1)
							 + inputFile.get(i).get(j+2))){
						inputFile.get(i).set(j,inputFile.get(i).get(j) + inputFile.get(i).get(j+1)
								+ inputFile.get(i).get(j+2));
						inputFile.get(i).remove(j+1);
						inputFile.get(i).remove(j+1);
					}
				}
				if(inputFile.get(i).get(j).length() == 3 && j+1 < inputFile.get(i).size()){
					if(word.keywordDic.contains(inputFile.get(i).get(j) + inputFile.get(i).get(j+1))){
						inputFile.get(i).set(j,inputFile.get(i).get(j) + inputFile.get(i).get(j+1));
						inputFile.get(i).remove(j+1);
					}
				}

			}
			
		}
		
		return inputFile;
	}
	/*
	 * 추가적인 붙임글자 처리
	 */
	public ArrayList<ArrayList<String>> mergeString_plus(ArrayList<ArrayList<String>> inputFile) {
		int arraySize = inputFile.size();
		Keyword word = new Keyword();
		String mergeString = "";
		String[] mark = { "⅓", "⅔", "·" };

		for (int i = 0; i < arraySize; i++) {
			
			for (int j = 0; j < inputFile.get(i).size(); j++) {
				try {
					mergeString += inputFile.get(i).get(j);
					if (inputFile.get(i).get(j).contains(mark[1])
							|| inputFile.get(i).get(j).contains(mark[0])
							|| inputFile.get(i).get(j).contains(mark[2])) {
						if (inputFile.get(i).get(j).length() == 1) {
							inputFile.get(i).set(
									j - 1,
									inputFile.get(i).get(j - 1)
											+ inputFile.get(i).get(j));
							inputFile.get(i).remove(j);
						} else if (inputFile.get(i).get(j).length() == 2) {
							inputFile.get(i).set(
									j - 1,
									inputFile.get(i).get(j - 1)
											+ inputFile.get(i).get(j));
							inputFile.get(i).remove(j);
						}
					}

				} catch (ArrayIndexOutOfBoundsException e) {

				}
			}
			// 7장읽을때만 필요한 부분, 1장읽을때는 주석으로 바꾸어야함
			
			try {
				if (inputFile.get(i).size() == 29) {
					inputFile.get(i).add(11, "-");
					inputFile.get(i).add(30, "-");
				} else if (inputFile.get(i).size() == 30
						&& inputFile.get(i).get(29).contains("投")) {
					inputFile.get(i).add(11, "-");
				} else if (inputFile.get(i).size() == 30
						&& inputFile.get(i).get(29).contains("投") == false) {
					inputFile.get(i).add(30, "-");
				}
			} catch (IndexOutOfBoundsException e) {
			}
			
			// System.out.print(mergeString);
			if (word.keywordDic.contains(mergeString)) {
				inputFile.get(i).clear();
				inputFile.get(i).add(mergeString);
				mergeString = "";
			} else
				mergeString = "";
		}

		return inputFile;
	}
	public ArrayList<ArrayList<String>> deleteString_ect(ArrayList<ArrayList<String>> inputFile){
		int arraySize = inputFile.size();
		for(int i=0; i<arraySize; i++){
			if(inputFile.get(i).contains("데뷔첫경기")){
				System.out.print(inputFile.get(i-1));
				int deleteCheck = 0;
				while(deleteCheck==1){
					System.out.print(inputFile.get(i-1));
				}		
			}
		}
		return inputFile;
	}
	/*
	 * 테스트를 위한 프린트 함수
	 */
	public void printarray(){
		for(int i = 0; i < inputString.size(); i++){
			System.out.println(inputString.get(i));
		}
	}
}
